#!/usr/bin/env bash

cd $1
PSQL_FILE=$(ls $(ls | awk -F '_' '{print $1}' | tail -n 1)*.psql.gz)
FILES_FILE=$(ls $(ls | awk -F '_' '{print $1}' | tail -n 1)*.tar.gz)

docker build -f $2 -t $3/$4 --build-arg psql_file=$PSQL_FILE --build-arg files_file=$FILES_FILE . || STATUS=$?

DEPLOY_USER=$(cat /home/gitlab-deploy/gitlab-user)
docker logout $3
cat /home/gitlab-deploy/gitlab-token | docker login -u $DEPLOY_USER --password-stdin $3
docker push $3/$4 || STATUS=$?
docker logout $3

docker rmi $3/$4 || STATUS=$?
exit $STATUS
