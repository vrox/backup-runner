FROM scratch
ARG psql_file
ARG files_file
COPY $psql_file .
COPY $files_file .
CMD [""]
